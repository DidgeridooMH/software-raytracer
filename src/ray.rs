use glm::Vec3;
use rand::{thread_rng, Rng};
use std::f32::consts::PI;

#[derive(Copy, Clone, Default)]
pub struct Ray {
    origin: Vec3,
    direction: Vec3,
}

impl Ray {
    pub fn new(origin: Vec3, direction: Vec3) -> Self {
        Self { origin, direction }
    }

    pub fn origin(&self) -> Vec3 {
        self.origin
    }

    pub fn dir(&self) -> Vec3 {
        self.direction
    }

    pub fn hit_cast<T>(&self, objects: &Vec<T>, min_distance: f32) -> Option<Hit>
    where
        T: Intersectable,
    {
        // Find the closest hit to the ray.
        let mut closest_hit: Option<Hit> = None;
        let mut closest_hit_dist: Option<f32> = None;
        for object in objects {
            if let Some(hit) = object.intersect(self, min_distance) {
                let distance = (hit.pos() - self.origin()).norm();
                if distance >= min_distance {
                    match closest_hit_dist {
                        Some(closest) => {
                            if distance < closest {
                                closest_hit_dist = Some(distance);
                                closest_hit = Some(hit);
                            }
                        }
                        _ => {
                            closest_hit = Some(hit);
                            closest_hit_dist = Some(distance);
                        }
                    }
                }
            }
        }

        closest_hit
    }

    pub fn cast<T>(
        &self,
        objects: &Vec<T>,
        min_distance: f32,
        light: &(Vec3, Vec3),
        indirect_sample_count: usize,
        depth: u32,
    ) -> Option<Hit>
    where
        T: Intersectable,
    {
        // Stop casting if we're out of bounces.
        if depth == 0 {
            return None;
        }

        match self.hit_cast(objects, min_distance) {
            Some(mut hit) => {
                /*
                 * Calculate the direct lighting component of the lighting equation.
                 * First, calculate if the hit position is in shadow by tracing a ray
                 * to the light source. Then, use the point light equation the dot
                 * product to the angle of the light.
                 */
                let light_dir = light.0 - hit.pos();
                let shadow_factor =
                    match Ray::new(hit.pos(), light_dir.normalize()).hit_cast(&objects, 0.0001) {
                        None => 1.0,
                        Some(h) => {
                            if (h.pos() - hit.pos()).norm() > light_dir.norm() {
                                1.0
                            } else {
                                0.0
                            }
                        }
                    };
                let direct_lighting = shadow_factor
                    * hit.normal().dot(&light_dir).max(0.0)
                    * glm::matrix_comp_mult(
                        &light.1,
                        &((1.0 / light_dir.norm().powi(2)) * hit.color()),
                    );

                let (nt, nb) = make_coordinate_space(&hit.normal());
                let normal_rotation = glm::mat3(
                    nt.x,
                    nt.y,
                    nt.z,
                    hit.normal().x,
                    hit.normal().y,
                    hit.normal().z,
                    nb.x,
                    nb.y,
                    nb.z,
                );

                let mut indirect_samples = Vec::with_capacity(indirect_sample_count);
                for _ in 0..indirect_sample_count {
                    let sample = normal_rotation * uniform_sample();
                    indirect_samples.push(
                        match Ray::new(hit.pos(), sample).cast(
                            &objects,
                            0.0001,
                            &light,
                            indirect_sample_count,
                            depth - 1,
                        ) {
                            Some(h) => h.color(),
                            None => glm::Vec3::zeros(),
                        },
                    );
                }

                let mut indirect_lighting = glm::Vec3::zeros();
                for sample in &indirect_samples {
                    indirect_lighting += sample;
                }
                indirect_lighting /= indirect_samples.len() as f32;

                let lighting = direct_lighting / PI + 2.0 * indirect_lighting;
                hit.color = glm::matrix_comp_mult(&lighting, &hit.color());
                Some(hit)
            }
            _ => None,
        }
    }
}

#[derive(Copy, Clone)]
pub struct Hit {
    position: Vec3,
    normal: Vec3,
    color: Vec3,
}

impl Hit {
    pub fn new(position: Vec3, normal: Vec3, color: Vec3) -> Self {
        Self {
            position,
            normal,
            color,
        }
    }

    pub fn pos(&self) -> Vec3 {
        self.position
    }

    pub fn normal(&self) -> Vec3 {
        self.normal
    }

    pub fn color(&self) -> Vec3 {
        self.color
    }
}

pub trait Intersectable {
    fn intersect(&self, ray: &Ray, bias: f32) -> Option<Hit>;
}

pub fn uniform_sample() -> glm::Vec3 {
    let mut rng = thread_rng();
    let r: (f32, f32) = (rng.gen_range(0.0..1.0), rng.gen_range(0.0..1.0));

    let sin_theta = (1.0 - r.0.powi(2)).sqrt();
    let phi = 2.0 * PI * r.1;
    let x = sin_theta * phi.cos();
    let z = sin_theta * phi.sin();
    glm::vec3(x, r.0, z)
}

pub fn make_coordinate_space(n: &glm::Vec3) -> (glm::Vec3, glm::Vec3) {
    let tangent = glm::vec3(n.z, 0.0, -n.x) / glm::vec2(n.x, n.z).norm();
    let bitangent = n.cross(&tangent);
    (tangent, bitangent)
}
