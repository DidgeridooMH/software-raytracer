use crate::{Cube, Ray};
use std::f32::consts::PI;
use std::io::Write;
use std::sync::{Arc, Mutex, RwLock};
use std::time::Duration;

type ImageBuffer = Arc<Mutex<Vec<f32>>>;

#[derive(Clone)]
pub struct Renderer {
    image_buffers: Vec<ImageBuffer>,
    width: u32,
    height: u32,
    num_of_threads: usize,
}

impl Renderer {
    pub fn new(width: u32, height: u32) -> Self {
        let total_pixels = width * height;
        let num_of_threads = std::thread::available_parallelism().unwrap().get();
        let bytes_per_thread = 4 * total_pixels as usize / num_of_threads;
        let mut image_buffers = Vec::new();
        for _ in 0..num_of_threads {
            image_buffers.push(Arc::new(Mutex::new(vec![0f32; bytes_per_thread])));
        }
        Self {
            image_buffers,
            width,
            height,
            num_of_threads,
        }
    }

    pub fn render(
        &mut self,
        pixels: &mut [u8],
        objects: &Arc<RwLock<Vec<Cube>>>,
        light: &(glm::Vec3, glm::Vec3),
        exposure: f32,
    ) {
        let rendered_pixels = Arc::new(RwLock::new(0usize));
        let mut threads = Vec::new();
        for (i, thread_image) in &mut self.image_buffers.iter_mut().enumerate() {
            let img = thread_image.clone();
            let objects = objects.clone();
            let width = self.width;
            let height = self.height;
            let light = *light;
            let rendered_pixels = rendered_pixels.clone();
            threads.push(std::thread::spawn(move || {
                Renderer::render_worker(i, img, width, height, objects, light, rendered_pixels);
            }));
        }

        let render_time = std::time::Instant::now();
        loop {
            let mut total_time = render_time.elapsed().as_secs();
            let hours = total_time / 3600;
            total_time -= hours * 3600;
            let minutes = total_time / 60;
            total_time -= minutes * 60;

            let rendered_pixels = *rendered_pixels.read().unwrap();
            print!(
                "\r{:0.2}% {:02}:{:02}:{:02}",
                (rendered_pixels as f32 / pixels.len() as f32) * 400.0,
                hours,
                minutes,
                total_time
            );
            std::io::stdout().flush().unwrap();
            if rendered_pixels == pixels.len() / 4 {
                println!(" [COMPLETE]");
                break;
            }
            std::thread::sleep(Duration::from_millis(200));
        }

        for thread in threads {
            thread.join().unwrap();
        }

        let bytes_per_thread = 4 * (self.width * self.height) as usize / self.num_of_threads;
        for i in 0..self.num_of_threads {
            let img = self.image_buffers[i].lock().unwrap();

            for n in 0..bytes_per_thread {
                if n % 4 != 4 {
                    pixels[i * bytes_per_thread + n] =
                        (Renderer::tonemap_channel(exposure * img[n]) * 255.0) as u8;
                } else {
                    pixels[i * bytes_per_thread + n] = 255u8;
                }
            }
        }
        println!("{} ms", render_time.elapsed().as_millis());
    }

    #[inline]
    fn tonemap_channel(color: f32) -> f32 {
        const GAMMA: f32 = 2.2;
        ((color * (2.51 * color + 0.03)) / (color * (2.43 * color + 0.59) + 0.14))
            .clamp(0.0, 1.0)
            .powf(1.0 / GAMMA)
    }

    fn render_worker(
        thread_id: usize,
        img: Arc<Mutex<Vec<f32>>>,
        width: u32,
        height: u32,
        objects: Arc<RwLock<Vec<Cube>>>,
        light: (glm::Vec3, glm::Vec3),
        rendered_pixels: Arc<RwLock<usize>>,
    ) {
        const LIGHT_BOUNCES: u32 = 4;
        const INDIRECT_SAMPLES: usize = 256;
        const FOV: f32 = 60.0;
        let view: f32 = (PI * 0.5 * FOV / 180.0).tan();
        let unit_width: f32 = 1.0 / width as f32;
        let unit_height: f32 = 1.0 / height as f32;
        let aspect_ratio: f32 = width as f32 / height as f32;

        let mut img = img.lock().unwrap();
        let pixels_per_thread = img.len() / 4;
        let thread_offset = thread_id * pixels_per_thread;
        for mut x in thread_offset..(thread_offset + pixels_per_thread) {
            let y = x / width as usize;
            let world_x = (2.0 * (((x as u32 % width) as f32 + 0.5) * unit_width) - 1.0)
                * view
                * aspect_ratio;
            let world_y = (1.0 - 2.0 * ((y as f32 + 0.5) * unit_height)) * view;
            let primary_ray = Ray::new(
                glm::vec3(0.0, 1.5, 5.0),
                glm::vec3(world_x, world_y, -1.0).normalize(),
            );

            // Cast a ray to check for an object on the screen.
            let color = match primary_ray.cast(
                &objects.read().unwrap(),
                0.0,
                &light,
                INDIRECT_SAMPLES,
                LIGHT_BOUNCES,
            ) {
                Some(hit) => hit.color(),
                _ => glm::vec3(0.0, 0.0, 0.0),
            };

            x -= thread_offset;
            img[x * 4] = color[2];
            img[x * 4 + 1] = color[1];
            img[x * 4 + 2] = color[0];
            img[x * 4 + 3] = 1.0;

            *rendered_pixels.write().unwrap() += 1;
        }
    }
}
