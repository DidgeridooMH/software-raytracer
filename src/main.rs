extern crate nalgebra_glm as glm;

mod ray;
use ray::*;
mod triangle;
use triangle::*;
mod render;
use render::Renderer;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use std::f32::consts::{FRAC_PI_2, FRAC_PI_6, PI};
use std::sync::{Arc, RwLock};

pub fn main() {
    const WIDTH: u32 = 800;
    const HEIGHT: u32 = 600;

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Software Raytracer", WIDTH, HEIGHT)
        .position_centered()
        .build()
        .unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut renderer = Renderer::new(WIDTH, HEIGHT);

    // TODO: Top and Bottom blocks don't process shadows correctly?
    let objects = Arc::new(RwLock::new(vec![
        Cube::with_transform(
            Transform::new()
                .translation(-0.6, 1.25, -0.5)
                .rotation(0.0, FRAC_PI_6, 0.0)
                .scale(1.0, 2.0, 1.0),
            glm::vec3(0.8, 0.8, 0.8),
        ),
        Cube::with_transform(
            Transform::new().scale(4.0, 0.5, 4.0).rotation(PI, 0.0, 0.0),
            glm::vec3(0.8, 0.8, 0.8),
        ),
        Cube::with_transform(
            Transform::new()
                .scale(4.1, 0.5, 4.1)
                .rotation(FRAC_PI_2, 0.0, 0.0)
                .translation(0.0, 2.0, -2.0),
            glm::vec3(0.8, 0.8, 0.8),
        ),
        Cube::with_transform(
            Transform::new()
                .scale(4.1, 0.5, 4.1)
                .translation(0.0, 4.0, 0.0)
                .rotation(PI, 0.0, 0.0),
            glm::vec3(0.8, 0.8, 0.8),
        ),
        Cube::with_transform(
            Transform::new()
                .scale(4.1, 0.5, 4.1)
                .translation(-2.0, 2.0, 0.0)
                .rotation(0.0, 0.0, FRAC_PI_2),
            glm::vec3(1.0, 0.2, 0.2),
        ),
        Cube::with_transform(
            Transform::new()
                .scale(4.1, 0.5, 4.1)
                .translation(2.0, 2.0, 0.0)
                .rotation(0.0, 0.0, FRAC_PI_2),
            glm::vec3(0.2, 1.0, 0.2),
        ),
        Cube::with_transform(
            Transform::new()
                .translation(0.6, 0.75, 0.5)
                .rotation(0.0, -FRAC_PI_6, 0.0),
            glm::vec3(0.8, 0.8, 0.8),
        ),
    ]));

    const EXPOSURE: f32 = 1.0;
    let light = (
        glm::vec3(0.0, 3.5, 0.0),
        1.2 * glm::vec3::<f32>(1.0, 1.0, 1.0),
    );
    for object in objects.write().unwrap().iter_mut() {
        object.apply_transform();
    }
    let mut surface = window.surface(&event_pump).unwrap();
    surface.with_lock_mut(|pixels| {
        renderer.render(pixels, &objects, &light, EXPOSURE);
    });
    surface.save_bmp("image.bmp").unwrap();
    surface.finish().unwrap();

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
        }
    }
}
