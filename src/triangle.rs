use crate::{Hit, Intersectable, Ray};
use glm::Vec3;

#[derive(Copy, Clone)]
pub struct Triangle {
    points: [Vec3; 3],
    color: Vec3,
    normal: Vec3,
}

impl Triangle {
    pub fn new(p0: Vec3, p1: Vec3, p2: Vec3, color: Vec3) -> Self {
        Self {
            points: [p0, p1, p2],
            color,
            normal: glm::triangle_normal(&p0, &p1, &p2),
        }
    }

    pub fn transform(&self, transform: &Transform) -> Self {
        let mut tri = *self;
        for p in &mut tri.points {
            *p = transform.transform(p);
        }
        tri.normal = glm::triangle_normal(&tri.points[0], &tri.points[1], &tri.points[2]);
        tri
    }
}

impl Intersectable for Triangle {
    fn intersect(&self, ray: &Ray, bias: f32) -> Option<Hit> {
        let e1 = self.points[1] - self.points[0];
        let e2 = self.points[2] - self.points[0];

        let p_vec = ray.dir().cross(&e2);
        let det = e1.dot(&p_vec);

        if det < 0.0 {
            return None;
        }

        let t_vec = ray.origin() - self.points[0];
        let u = t_vec.dot(&p_vec) / det;
        if u < 0.0 || u > 1.0 {
            return None;
        }

        let q = t_vec.cross(&e1);
        let v = ray.dir().dot(&q) / det;
        if v < 0.0 || (u + v) > 1.0 {
            return None;
        }

        let t = e2.dot(&q) / det;

        if t < bias {
            return None;
        }

        Some(Hit::new(
            ray.origin() + t * ray.dir(),
            self.normal,
            self.color,
        ))
    }

    /*fn intersect(&self, ray: &Ray, bias: f32) -> Option<Hit> {
        let view_factor = self.normal.dot(&ray.dir());
        if view_factor.abs() > 0.0001 {
            let d = -self.normal.dot(&self.points[0]);
            let t = -(self.normal.dot(&ray.origin()) + d) / view_factor;
            if t > 0.0 {
                let hit_position = ray.origin() + t * ray.dir();
                for i in 0..3 {
                    let tri_edge = self.points[(i + 1) % self.points.len()] - self.points[i];
                    let point_edge = hit_position - self.points[i];
                    let c = tri_edge.cross(&point_edge);
                    if self.normal.dot(&c) < 0.0 {
                        return None;
                    }
                }

                if t > bias {
                    return Some(Hit::new(hit_position, self.normal, self.color));
                }
            }
        }
        None
    }*/
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Transform {
    translation: Vec3,
    rotation: Vec3,
    scale: Vec3,
}

impl Transform {
    pub fn new() -> Self {
        Self {
            translation: Vec3::zeros(),
            rotation: Vec3::zeros(),
            scale: glm::vec3(1.0, 1.0, 1.0),
        }
    }

    pub fn translation(&mut self, x: f32, y: f32, z: f32) -> &mut Self {
        self.translation = glm::vec3(x, y, z);
        self
    }

    pub fn rotation(&mut self, x: f32, y: f32, z: f32) -> &mut Self {
        self.rotation = glm::vec3(x, y, z);
        self
    }

    pub fn scale(&mut self, x: f32, y: f32, z: f32) -> &mut Self {
        self.scale = glm::vec3(x, y, z);
        self
    }

    pub fn transform(&self, v: &Vec3) -> glm::Vec3 {
        (glm::translation(&self.translation)
            * glm::rotation(self.rotation.z, &glm::vec3(0.0, 0.0, 1.0))
            * glm::rotation(self.rotation.y, &glm::vec3(0.0, 1.0, 0.0))
            * glm::rotation(self.rotation.x, &glm::vec3(1.0, 0.0, 0.0))
            * glm::scaling(&self.scale)
            * glm::vec4(v[0], v[1], v[2], 1.0))
        .xyz()
    }
}

pub struct Cube {
    tris: Vec<Triangle>,
    transformed_tris: Vec<Triangle>,
    transform: Transform,
}

impl Cube {
    pub fn with_transform(transform: &Transform, color: Vec3) -> Self {
        let points = vec![
            glm::vec3(-0.5, -0.5, 0.5),
            glm::vec3(0.5, -0.5, 0.5),
            glm::vec3(0.5, 0.5, 0.5),
            glm::vec3(-0.5, 0.5, 0.5),
            glm::vec3(-0.5, -0.5, -0.5),
            glm::vec3(0.5, -0.5, -0.5),
            glm::vec3(0.5, 0.5, -0.5),
            glm::vec3(-0.5, 0.5, -0.5),
        ];

        let tris = vec![
            // Front
            Triangle::new(points[0], points[1], points[2], color),
            Triangle::new(points[0], points[2], points[3], color),
            // Right
            Triangle::new(points[2], points[1], points[6], color),
            Triangle::new(points[1], points[5], points[6], color),
            // Back
            Triangle::new(points[7], points[5], points[4], color),
            Triangle::new(points[7], points[6], points[5], color),
            // Left
            Triangle::new(points[3], points[4], points[0], color),
            Triangle::new(points[7], points[4], points[3], color),
            // Top
            Triangle::new(points[7], points[2], points[6], color),
            Triangle::new(points[7], points[3], points[2], color),
            //Bottom
            Triangle::new(points[0], points[4], points[1], color),
            Triangle::new(points[1], points[4], points[5], color),
        ];

        Self {
            tris: tris.clone(),
            transformed_tris: tris,
            transform: *transform,
        }
    }

    pub fn apply_transform(&mut self) {
        for i in 0..self.tris.len() {
            self.transformed_tris[i] = self.tris[i].transform(&self.transform);
        }
    }

    pub fn prims(&self) -> &Vec<Triangle> {
        &self.transformed_tris
    }
}

impl Intersectable for Cube {
    fn intersect(&self, ray: &Ray, bias: f32) -> Option<Hit> {
        ray.hit_cast(self.prims(), bias)
    }
}
